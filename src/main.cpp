#include "matrix.hpp"

void usage(){
    printf("\n");
    printf("Matrix++ usage help:\n");
    for (int i = 0; i < 20; i++)
    {
        printf("=");
    }
    printf("\n");
    printf("    -h (--help) -- display usage help\n");
    printf("    -c (--color) -- set main color\n");
    printf("        Available colors: green, blue, default\n");
    printf("    -s (--speed) -- set flow speed (second)\n");
    printf("    -a (--ascii) -- set charset\n");
    printf("\n");
}

int main(int argc, char **argv)
{
    srand(time(NULL));
    unsigned int WIDTH, HEIGHT;
    struct winsize size;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &size);
    WIDTH = size.ws_col;
    HEIGHT = size.ws_row;
    string charset = "01";
    // Get user options
    string color = "default";
    float speed = 0.05;
    int option;
    while((option = getopt(argc, argv, ":hc:s:a:")) != -1)
    {
        switch(option)
        {
            case 'h':
                usage();
                exit(0);
                break;
            case 'c':
                color = optarg;
                break;
            case 's':
                speed = atof(optarg);
                break;
            case 'a':
                charset = optarg;
                break;
        }
    }
    matrix(HEIGHT, WIDTH, color, speed, charset);
    return 0;
}
